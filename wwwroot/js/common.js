
/* 페이지 요소들이 읽혔을 때 실행되는 스크립트 */
$(document).ready(function(){

	var floatPosition = parseInt($(".floatMenu").css('top'));
	$(window).scroll(function() {
		var scrollTop = $(window).scrollTop();
		var newPosition = scrollTop + floatPosition + "px";
		$(".floatMenu").stop().animate({"top" : newPosition}, 500);
	});

	var placeholderTarget = $(".input_box input, .input_box textarea");
    placeholderTarget.focus(function(){
        $(this).siblings("label").fadeOut("fast");
    });
    placeholderTarget.focusout(function(){
        if($(this).val() == ""){
            $(this).siblings("label").fadeIn("fast");
        }
    });

	$('.target').click(function(){
		var target = $(this).data("target");
		var target = $(target).offset().top;
		$('body, html').animate({scrollTop : target}, 1000)
	});

	$(document).on('focus', '.input_text', function(){
		$('.mo_fixedbar').addClass('hidden');
	});
	$(document).on('focusout', '.input_text', function(){
		$('.mo_fixedbar').removeClass('hidden');
	});


});

/* 마우스 스크롤 함수 */
$(window).on('scroll', function() {
	scroll_class();
});

/* 스크롤 시 animation 클래스를 찾아서 active 클래스를 추가하면서 동시에 animation 클래스를 제거 */
function scroll_class(){
	$('.animation').each(function() {
		var win_half = $(window).height()*2/10;
		if($(window).scrollTop() >= $(this).offset().top - win_half) {
			$(this).addClass('active');
			var id = $(this).data('nav');
            $('[data-target*="pc_section"]').parents('li').removeClass('active');
            $('[data-target='+ id +']').parents('li').addClass('active');
        }
    });
};
$(window).load(function(){

	$('.pc_fixedbar').removeClass('hidden');	
	scroll_class();

	$('.form_submit').on('click' , function(){
		if(!$('#agree').prop('checked')){
			alert('개인정보취급방침에 동의해주세요.,');
			return false;
		}
		
		var validate = validateForm('.landing_form');
		
		if(validate ==false){
			return false;
		}
		$('.pc_lodingbar').addClass('view');

		var temp = [];
		var user_name = document.getElementsByName('user_name[]');
		var grade = document.getElementsByName('grade[]');
		
		for(i=0;i<user_name.length; i++) {
			temp.push({name : user_name[i].value , grade : grade[i].value});
		}
		//console.log(temp);
		var result = { };
		$.each($('.landing_form').serializeArray(), function() {
			result[this.name] = this.value;
		});
		var data =	{
			"parent_phonenumber": result['parent_phonenumber'],
			"parent_which": result['parent_which'],
			"students":temp
		}
		//console.log(data);
		//return false;
		$.ajax({
			url: "https://ct.hasangedu.com/api/api.php?action=createlead",
			type: "post",
			contentType: "application/json",
			data:  JSON.stringify(data),
			dataType: "json",
			success: function(data) {
				if(data.result === "ok"){
					alert('신청해주셔서 감사합니다.');
					window.location.reload();
				}else{
					alert('[오류] 다시 작성해주세요');
					$('.pc_lodingbar').removeClass('view');
				}
			},
			error: function(jqXHR,textStatus,errorThrown) {
				alert('서버장애로 인해 연락처로 문의부탁드립니다.');
				//console.log(jqXHR);
			}
		});
		
	});
	$('.form_submit2').on('click' ,function(){
		if(!$('#agree2').prop('checked')){
			alert('개인정보취급방침에 동의해주세요.,');
			return false;
		}
		var validate = validateForm('.landing_form2');

		if(validate ==false){
			return false;
		}

		$('.mo_lodingbar').addClass('view');

		var temp = [];
		var user_name = document.getElementsByName('mo_user_name[]');
		var grade = document.getElementsByName('mo_grade[]');
		
		for(i=0;i<user_name.length; i++) {
			temp.push({name : user_name[i].value , grade : grade[i].value});
		}
		var result = { };
		$.each($('.landing_form2').serializeArray(), function() {
			result[this.name] = this.value;
		});
		var data =	{
			"parent_phonenumber": result['parent_phonenumber'],
			"parent_which": result['parent_which'],
			"students":temp
		}
		//console.log(data);
		//return false;
		$.ajax({
			url: "https://ct.hasangedu.com/api/api.php?action=createlead",
			type: "post",
			contentType: "application/json",
			data:  JSON.stringify(data),
			dataType: "json",
			success: function(data) {
				if(data.result === "ok"){
					alert('신청해주셔서 감사합니다.');
					window.location.reload();
				}else{
					alert('[오류] 다시 작성해주세요');
					$('.mo_lodingbar').removeClass('view');
				}
			},
			error: function(jqXHR,textStatus,errorThrown) {
				alert('서버장애로 인해 연락처로 문의부탁드립니다.');
				//console.log(jqXHR);
			}
		});
	});

	function validateForm($formSelector){
		if(typeof($formSelector)=='string'){
			var $childForms = $($formSelector+' .form');
		} else {
			var $childForms =$formSelector.find('.form');
		}
		
		var result = true;
		$childForms.each(function(){
			var type = $(this).data('type');
			var essential = $(this).data('essential');
			var message = $(this).data('message');
			var value = $.trim($(this).val());
			switch(type){
				default : 
					if(value==''){
						result = false;
						alert(message);
						$(this).focus();
						return false;
					}
					break;
				case 'email' :
					if(value==''){
						result = false;
						alert('이메일을 입력해주세요.');
						$(this).focus();
						return false;
					}
					var regex=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/;   
					if(regex.test(value) === false) {  
						result = false;
						alert("잘못된 이메일 형식입니다.");
						$(this).focus();
						return false;  
					} 
					break;
				case 'id' :
					if(value==''){
						result = false;
						alert('아이디를 입력해주세요.');
						$(this).focus();
						return false;
					}
					var regex=  /^[a-z0-9_]{5,15}$/;

					if(regex.test(value) === false) { 
				
						result = false;
						alert("아이디는 소문자로 시작하는  5~15자 소문자, 숫자의 조합이어야 합니다.");
						$(this).focus();
						return false;  
					} 
					break;
				case 'password' :
					if(value==''){
						result = false;
						alert('비밀번호를 다시한번 확인해주세요.');
						$(this).focus();
						return false;
					}
					var regex=  /^[a-z0-9_]{5,15}$/;

					if(regex.test(value) === false) { 
				
						result = false;
						alert("아이디는 소문자로 시작하는  5~15자 소문자, 숫자의 조합이어야 합니다.");
						$(this).focus();
						return false;  
					} 
					break;
				case 'number' :
					if(value==''){
						result = false;
						alert(message);
						$(this).focus();
						return false;
					}
					var regex=  /^[0-9]+$/;
					if(regex.test(value) === false) { 
				
						result = false;
						alert("숫자만 입력 가능합니다.");
						$(this).focus();
						return false;  
					} 
					break;
				case 'exception' :
					var exception = $(this).data('exception');
					var check = validation[exception](value);
					if(!check['result']){
						alert(check['message']);
						result = false;
					}
					break;
			}
		});
		
		return result;
	}
})

function get_hasangedy_list() {
	var queryString = $("form[name=testForm]").serialize();
	$.ajax({
		type : 'post',
		url : '/test.php',
		data : queryString,
		dataType : 'json',
		error: function(json){
			alert(json);
		},

		success : function(json){
			alert(json);
		}
	});
}

/* 자동으로 하이픈 넣기 */
$(function () {
   $('[name=parent_phonenumber]').keyup(function() {
      
      var phone = '';
      var seoul = 0;
      var string = $(this).val();
	  var regex =  /^[0-9\-]+$/;

	  if(!regex.test(string)){
		  alert('숫자만 입력 할 수 있습니다.');
		  $(this).val('');
		  return false;
	  }

      var value = string.replace(/[^0-9]/g, '');

      /* 서울 앞자리가 02 일때 */
      if(value.substring(0,2) == '02'){
         seoul = 1;
      }

      /* 서울번호일때 글자크기 제한 다르게 설정 */
      if(seoul == 0){
         $(this).attr({'maxlength':'13'});
      } else if(seoul == 1){
         $(this).attr({'maxlength':'11'});
      }

      /* 자동으로 하이픈 삽입하기 */
      if(value.length > (3-seoul) && value.length <= 7){
         phone += value.substr(0, (3-seoul));
         phone += "-";
         phone += value.substr(3-seoul);
         $(this).val(phone);
      } else if(value.length > (7-seoul)){
         phone += value.substr(0, (3-seoul));
         phone += "-";
         phone += value.substr(3-seoul, 4-seoul);
         phone += "-";
         phone += value.substr(7-seoul-seoul);
         $(this).val(phone);
      }
   });
});